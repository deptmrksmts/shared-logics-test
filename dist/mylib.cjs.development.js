'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = require('react');
var React__default = _interopDefault(React);

var ApiContext =
/*#__PURE__*/
React__default.createContext({});
var ApiContextConsumer = ApiContext.Consumer;
var useApi = function useApi() {
  return React.useContext(ApiContext);
}; // add ApiContextProvider to ContextProvider

var ApiContextProvider = function ApiContextProvider(props) {
  var getPokemon = function getPokemon() {
    try {
      return Promise.resolve(fetch('https://pokeapi.co/api/v2/pokemon/ditto').then(function (res) {
        return res.json();
      })).then(function (result) {
        setPokemon(result);
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  var _useState = React.useState(),
      pokemon = _useState[0],
      setPokemon = _useState[1];

  var store = {
    pokemon: pokemon,
    getPokemon: getPokemon
  };
  return React__default.createElement(ApiContext.Provider, Object.assign({}, props, {
    value: store
  }));
};

var Thing = function Thing() {
  return React.createElement("div", null, "the snozzberries taste like snozzberries");
};
var colorPallete = {
  blue: 'blue'
};

exports.ApiContext = ApiContext;
exports.ApiContextConsumer = ApiContextConsumer;
exports.ApiContextProvider = ApiContextProvider;
exports.Thing = Thing;
exports.colorPallete = colorPallete;
exports.useApi = useApi;
//# sourceMappingURL=mylib.cjs.development.js.map
