import React from 'react';
export interface ApiContextStore {
    pokemon: any;
    getPokemon: () => void;
}
export declare const ApiContext: React.Context<ApiContextStore>;
export declare const ApiContextConsumer: React.ExoticComponent<React.ConsumerProps<ApiContextStore>>;
export declare const useApi: () => ApiContextStore;
export declare const ApiContextProvider: React.FC;
