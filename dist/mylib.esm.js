import React__default, { useContext, useState, createElement } from 'react';

var ApiContext =
/*#__PURE__*/
React__default.createContext({});
var ApiContextConsumer = ApiContext.Consumer;
var useApi = function useApi() {
  return useContext(ApiContext);
}; // add ApiContextProvider to ContextProvider

var ApiContextProvider = function ApiContextProvider(props) {
  var getPokemon = function getPokemon() {
    try {
      return Promise.resolve(fetch('https://pokeapi.co/api/v2/pokemon/ditto').then(function (res) {
        return res.json();
      })).then(function (result) {
        setPokemon(result);
      });
    } catch (e) {
      return Promise.reject(e);
    }
  };

  var _useState = useState(),
      pokemon = _useState[0],
      setPokemon = _useState[1];

  var store = {
    pokemon: pokemon,
    getPokemon: getPokemon
  };
  return React__default.createElement(ApiContext.Provider, Object.assign({}, props, {
    value: store
  }));
};

var Thing = function Thing() {
  return createElement("div", null, "the snozzberries taste like snozzberries");
};
var colorPallete = {
  blue: 'blue'
};

export { ApiContext, ApiContextConsumer, ApiContextProvider, Thing, colorPallete, useApi };
//# sourceMappingURL=mylib.esm.js.map
