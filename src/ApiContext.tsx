import React, { useContext, useState } from 'react'
export interface ApiContextStore {
    pokemon: any
    getPokemon: () => void
}

export const ApiContext = React.createContext({} as ApiContextStore)
export const ApiContextConsumer = ApiContext.Consumer
export const useApi = () => useContext(ApiContext)

// add ApiContextProvider to ContextProvider
export const ApiContextProvider: React.FC = props => {
  const [pokemon, setPokemon] = useState()

  async function getPokemon() {
    const result = await fetch('https://pokeapi.co/api/v2/pokemon/ditto').then(res => res.json())
    setPokemon(result)
  }

  const store: ApiContextStore = {
    pokemon,
    getPokemon,
  }

  return <ApiContext.Provider {...props} value={store} />
}
